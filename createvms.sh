#!/bin/bash

function setFileNameHdd() {
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        hddPath=$baseFolder/$vmName/$vmName-hdd.vdi
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        hddPath=$baseFolder/$vmName/$vmName-hdd.vdi
    elif [[ "$OSTYPE" == "cygwin" ]]; then
        # POSIX compatibility layer and Linux environment emulation for Windows
        hddPath=$baseFolder/$vmName/$vmName-hdd.vdi
    elif [[ "$OSTYPE" == "msys" ]]; then
        # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
        hddPath=$baseFolder\\$vmName\\$vmName-hdd.vdi
    elif [[ "$OSTYPE" == "win32" ]]; then
        # It wasn't tested yet
        hddPath=$baseFolder\\$vmName\\$vmName-hdd.vdi
    elif [[ "$OSTYPE" == "freebsd"* ]]; then
        hddPath=$baseFolder/$vmName/$vmName-hdd.vdi
    else
        echo "Operative System: Unknown"
    fi
    echo "$hddPath"
}

# Define variables
vmName=$1       #ubuntuserver18
osType=$2       #Ubuntu_64
memorySize=$3   #512
hddSize=$4      #10240
baseFolder=$5   #D:\\VMs
isoPath=$6      #D:\\ubuntu-18.04.4-live-server-amd64.iso
fileNameHDD="$(setFileNameHdd)"

# VBox Create Ops
echo "Create Ubuntu Server VM"
VBoxManage createvm --name $vmName --ostype $osType --register --basefolder $baseFolder --default
echo "Set memory to $memorySize MB"
VBoxManage modifyvm $vmName --memory $memorySize
echo "Set 1 CPU"
VBoxManage modifyvm $vmName --cpus 1
echo "Create a Hard Disk with $hddSize MB"
VBoxManage createhd --filename $fileNameHDD --size $hddSize --format VDI
echo "Attach the Hard Disk"
VBoxManage storageattach $vmName --storagectl SATA --port 0 --device 0 --type hdd --medium $fileNameHDD
echo "Set a Optical Drive"
VBoxManage storageattach $vmName --storagectl IDE --port 1 --device 0 --type dvddrive --medium $isoPath