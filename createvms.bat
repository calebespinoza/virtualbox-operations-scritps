#!/bin/bash

SET vmName=ubuntu-server18
SET osType=Ubuntu_64
SET memorySize=512
SET hddSize=10240
SET baseFolder=D:\\VMs
SET fileNameHHDD=%baseFolder%\%vmName%\%vmName%-hdd.vdi

echo "Create Ubuntu Server VM"
VBoxManage.exe createvm --name %vmName% --ostype %osType% --register --basefolder %baseFolder% --default
echo "Set memory to 512 MB"
VBoxManage.exe modifyvm %vmName% --memory %memorySize%
echo "Set 1 CPU"
VBoxManage.exe modifyvm %vmName% --cpus 1
echo "Create a Hard Disk"
VBoxManage.exe createhd --filename %fileNameHHDD% --size %hddSize% --format VDI
echo "Attach the Hard Disk"
VBoxManage.exe storageattach %vmName% --storagectl SATA --port 0 --device 0 --type hdd --medium %fileNameHHDD%
echo "Set a Optical Drive"
VBoxManage.exe storageattach %vmName% --storagectl IDE --port 1 --device 0 --type dvddrive --medium D:\\ubuntu-18.04.4-live-server-amd64.iso
